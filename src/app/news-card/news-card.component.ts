import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.scss']
})
export class NewsCardComponent implements OnInit {

  @Input() newsCardData: any;
  constructor() { }

  ngOnInit() {
  }

  openNewTab(url: string) {
    window.open(url, '_blank');
  }

}
