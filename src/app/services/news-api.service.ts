import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NewsApiService {
  private dataUrl = 'https://api.myjson.com/bins/';
  private newsUrl = 'https://newsapi.org/v2/top-headlines';
  private apiKey = 'a682753c7acd476fb7af11f3eed8f016';

  constructor(private httpClient: HttpClient, private http: Http) {

  }

  getMainCat() {
    return this.http.get(this.dataUrl + 'qk7qi').pipe(map(res => res.json()));
  }

  getSubCat(id: number) {
    return this.http.get(this.dataUrl + '6frhm?cat=' + id).pipe(map(res => res.json()));
  }

  getNews(country: string, category: string, page: number) {
    // tslint:disable-next-line:max-line-length
    return this.http.get(this.newsUrl + '?country=' + country + '&category=' + category + '&page=' + page + '&apiKey=' + this.apiKey).pipe(map(res => res.json()));
  }
}
