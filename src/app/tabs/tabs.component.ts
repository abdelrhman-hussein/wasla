import { Component, OnInit, HostListener } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material';
import { NewsApiService } from '../services/news-api.service';

// -------------------------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------------------
// Create Interfaces to return data from api in It
// --------------------------------------------------------------------------------------------------------------
export interface MainNews {
  id: number;
  name: string;
}

export interface SubMainNews {
  id: number;
  name: string;
  slug: string;
}

export interface NewsSource {
  id: number;
  name: string;
}

export interface NewsList {
  author: number;
  content: number;
  description: number;
  publishedAt: number;
  source: NewsSource;
  title: number;
  url: number;
  urlToImage: number;
}


// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})

export class TabsComponent implements OnInit {

  // --------------------------------------------------------------------------------------------------------------
  // UI Variables
  // --------------------------------------------------------------------------------------------------------------
  // tslint:disable-next-line:no-inferrable-types
  showSecondHeader: boolean = false;
  // tslint:disable-next-line:no-inferrable-types
  showNews: boolean = false;
  // --------------------------------------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------------------------------------

  // --------------------------------------------------------------------------------------------------------------
  // LIST Variables
  // --------------------------------------------------------------------------------------------------------------
  mainCategoryList: Array<MainNews> = [];
  subMainCategoryList: Array<SubMainNews> = [];
  allNewsList: Array<NewsList> = [];

  newsPageNumber = 1;

  lastSub: string;
  // --------------------------------------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------------------------------------

  constructor(private api: NewsApiService) {
    this.getAllCat();
  }

  ngOnInit() {
  }

  // --------------------------------------------------------------------------------------------------------------
  // RETURN ALL MAIN CATEGORIES FUNCTION
  // --------------------------------------------------------------------------------------------------------------
  getAllCat() {
    this.api.getMainCat().subscribe(
      next => {
        this.mainCategoryList = next;
        this.getAllSubCat(this.mainCategoryList[0].id);
      },
      error => { },
      () => { }
    );
  }
  // --------------------------------------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------------------------------------


  // --------------------------------------------------------------------------------------------------------------
  // RETURN ALL SUB CATEGORIES FUNCTION
  // --------------------------------------------------------------------------------------------------------------
  getAllSubCat(id: number) {
    this.api.getSubCat(id).subscribe(
      next => {
        this.subMainCategoryList = next;
        this.showSecondHeader = true;
        this.lastSub = this.subMainCategoryList[0].slug;
        this.getAllNews(this.subMainCategoryList[0].slug, this.newsPageNumber);
      },
      error => { },
      () => { }
    );
  }
  // --------------------------------------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------------------------------------

  // --------------------------------------------------------------------------------------------------------------
  // RETURN ALL NEWS BY SUB CATEGORIES FUNCTION
  // --------------------------------------------------------------------------------------------------------------
  getAllNews(cat: string, page: number) {
    this.api.getNews('eg', cat, page).subscribe(
      next => {
        this.allNewsList = next.articles;

        setTimeout(() => {
          this.showNews = true;
        }, 200);
      },
      error => { },
      () => { }
    );
  }
  // --------------------------------------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------------------------------------

  // --------------------------------------------------------------------------------------------------------------
  // Update NEWS BY SUB CATEGORIES FUNCTION
  // --------------------------------------------------------------------------------------------------------------
  updateNews(cat: string, page: number) {
    this.api.getNews('eg', cat, page).subscribe(
      next => {
        // this.allNewsList.concat(next.articles);
        Array.prototype.push.apply(this.allNewsList, next.articles);

        setTimeout(() => {
          this.showNews = true;
        }, 200);
      },
      error => { },
      () => { }
    );
  }
  // --------------------------------------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------------------------------------


  // --------------------------------------------------------------------------------------------------------------
  // INFINITE SCROLL LOAD
  // --------------------------------------------------------------------------------------------------------------
  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    const pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    const max = document.documentElement.scrollHeight;
    // pos/max will give you the distance between scroll bottom and and bottom of screen in percentage.
    if (pos >= max - 25) {
      this.newsPageNumber++;
      this.updateNews(this.lastSub, this.newsPageNumber);
    }
  }
  // --------------------------------------------------------------------------------------------------------------
  // --------------------------------------------------------------------------------------------------------------
  changeMainCat(event: MatTabChangeEvent) {
    this.showSecondHeader = false;
    this.newsPageNumber = 1;
    this.getAllSubCat(this.mainCategoryList[event.index].id);
  }

  changeSubCat(event: MatTabChangeEvent) {
    this.showNews = false;
    this.newsPageNumber = 1;
    this.lastSub = this.subMainCategoryList[event.index].slug;
    this.getAllNews(this.subMainCategoryList[event.index].slug, this.newsPageNumber);
  }

  chnageHeader(event: MatTabChangeEvent) {
    if (event.index === 0) {
      this.showSecondHeader = true;
    }
  }

}
